#include <iostream>
#include "capsule.hpp"

int main()
{
  try
  {
    detail::Capsule container_;
    std::string str;
    while (std::getline(std::cin, str))
    {
      container_.addFromLine(str);
    }
    container_.customSort();
    container_.display();
  }

  catch (std::exception &e)
  {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  return 0;
}
