#ifndef DATA_HPP
#define DATA_HPP

#include <string>
#include <vector>
#include <regex>

struct DataStruct
{
  int key1;
  int key2;
  std::string str;
};

namespace detail
{
  class Capsule
  {
    std::vector<DataStruct> data_;
  public:
    Capsule();

    void add(const int &key1, const int &key2, const std::string &str);
    void addFromLine(const std::string &line);
    void display();
    void customSort();

  private:
    bool outOfRange(const int &key);
  };
}

#endif
