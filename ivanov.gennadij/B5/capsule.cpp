#include "capsule.hpp"
#include <cstring>
#include <string>
#include <iostream>
#include <algorithm>
#include <sstream>

detail::Capsule::Capsule()
{
}

void detail::Capsule::add(const int &key1, const int &key2, const std::string &str)
{
  data_.push_back({key1, key2, str}); 
}

void detail::Capsule::addFromLine(const std::string &line)
{
  std::istringstream temp(line);
  temp >> std::skipws;
  int key1 = 0;
  int key2 = 0;
  std::string str = {};
  char delimiter1;
  char delimiter2;
  if (temp)
  {
    temp >> key1 >> delimiter1;
    if (temp.bad() || temp.fail() || delimiter1 != ',' || outOfRange(key1))
    {
      throw std::invalid_argument("invalid argument\n");
    }
    temp >> key2 >> delimiter2 >> str;
    if (temp.bad() || temp.fail() || delimiter2 != ',' || outOfRange(key2))
    {
      throw std::invalid_argument(line);
    }
    if (!temp.eof())
    {
      std::string residualWords;
      temp >> std::noskipws;
      std::getline(temp, residualWords);
      str += residualWords; 
    }
  }
  add(key1, key2, str);
}

void detail::Capsule::display()
{
  if (!data_.empty())
  {
    std::for_each(data_.begin(), data_.end(), [](const DataStruct &data) {
      std::cout << data.key1 << "," << data.key2 << "," << data.str << std::endl;
    });
  }
}

void detail::Capsule::customSort()
{
  if (!data_.empty())
  {
    std::sort(data_.begin(), data_.end(), [](DataStruct &left, DataStruct &right)
    {
      if (left.key1 < right.key1)
      {
        return true;
      }
      else if (left.key1 == right.key1)
      {
        if (left.key2 < right.key2)
        {
          return true;
        }
        else if (left.key2 == right.key2)
        {
          return (left.str.size() < right.str.size());
        }
        else
        {
          return false;
        }
      }
      else
      {
        return false;
      }
    });
  }
}

bool detail::Capsule::outOfRange(const int &key)
{
  return key < -5 || key > 5;
}
