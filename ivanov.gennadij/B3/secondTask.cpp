#include <list>
#include <iterator>
#include "tasks.hpp"

void detail::second(std::istream &in, std::ostream &out)
{
  std::list<int> list_;
  int i;
  while (in >> i && list_.size() < 20 && !in.eof())
  {
    if (i >= 1 && i <= 20)
    {
      list_.push_back(i);
    }
    else
    {
      throw std::invalid_argument("Too large or too small");
    }
  }
  if (!in.eof())
  {
    throw std::invalid_argument("Wrong input sequence");
  }
  if (!list_.empty())
  {
    auto first = list_.begin();
    auto last = --list_.end();
    for (std::size_t i = 0; i < list_.size() / 2; ++i)
    {
      out << *(first++) << " " << *(last--) << " ";
    }
    if (first == last)
    {
      out << *first;
    }
    out << std::endl;
  }
}
