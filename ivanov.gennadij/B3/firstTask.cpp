#include <string>
#include "tasks.hpp"
#include "QueueWithPriority.hpp"
#include "CommandParser.hpp"

void detail::first(std::istream &in, std::ostream &out)
{
  std::string input;
  detail::QueueWithPriority<std::string> pr_q;
  while (std::getline(in, input))
  {
    detail::commandSet command = detail::getCommand(input);
    detail::commandParser(command, pr_q, out);
  }
}
