#ifndef TASKS_HPP
#define TASKS_HPP

#include <iostream>

namespace detail
{
  void first(std::istream &in, std::ostream &out);
  void second(std::istream &in, std::ostream &out);
}

#endif
