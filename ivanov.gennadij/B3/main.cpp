#include <string>
#include "tasks.hpp"

int main(int argc, char *argv[])
{
  if (argc != 2)
  {
    std::cerr << "Not enough or too many arguments" << std::endl;
    return 1;
  }
  uint8_t taskNumber_;
  std::size_t error_point;
  try
  {
    taskNumber_ = std::stoi(argv[1], &error_point);
  }
  catch (std::exception &e)
  {
    std::cerr << e.what() << std::endl; 
  }
  switch (taskNumber_)
  {
    case 1: 
    {
      detail::first(std::cin, std::cout);
      break;
    }
    case 2:
    {
      try
      {
        detail::second(std::cin, std::cout);
      }
      catch (std::exception &e)
      {
        std::cerr << e.what() << std::endl;
        return 1;
      }
      break;
    }
    default:
    {
      std::cerr << "Wrong parameter" << std::endl;
      return 1;
    }
  }
  return 0;
}
