#ifndef QUEUE_WITH_PRIORITY_HPP
#define QUEUE_WITH_PRIORITY_HPP

#include <array>
#include <deque>
#include <utility>
#include <algorithm>

namespace detail
{
  typedef enum
  {
    LOW,
    NORMAL,
    HIGH
  } ElementPriority;

  template<typename Element>
  class QueueWithPriority
  {
    public:

    QueueWithPriority();

    QueueWithPriority(const QueueWithPriority &rhs);

    QueueWithPriority(QueueWithPriority &&rhs) noexcept;

    ~QueueWithPriority();

    QueueWithPriority &operator=(const QueueWithPriority &rhs);

    QueueWithPriority &operator=(QueueWithPriority &&rhs) noexcept;

    void PutElementToQueue(const Element &element, ElementPriority priority);

    bool isEmpty() const noexcept;

    void clear() noexcept;

    Element GetElementFromQueue();

    void Accelerate();

    private:
    std::array<std::deque<Element>, 3> container_;
  };

  template<typename Element>
  QueueWithPriority<Element>::QueueWithPriority()
  {}

  template<typename Element>
  QueueWithPriority<Element>::QueueWithPriority(const QueueWithPriority &rhs):
    container_(rhs.container_)
  {
  }

  template<typename Element>
  QueueWithPriority<Element>::QueueWithPriority(QueueWithPriority &&rhs) noexcept:
    container_(std::move(rhs.container_))
  {
  }

  template<typename Element>
  QueueWithPriority<Element>::~QueueWithPriority()
  {}

  template<typename Element>
  QueueWithPriority<Element> &QueueWithPriority<Element>::operator=(const QueueWithPriority &rhs)
  {
    std::copy(rhs.container_.begin(), rhs.container_.end(), container_.begin());
    return *this;
  }

  template<typename Element>
  QueueWithPriority<Element> &QueueWithPriority<Element>::operator=(QueueWithPriority &&rhs) noexcept
  {
    std::move(rhs.container_.begin(), rhs.container_.end(), container_.begin());
    return *this;
  }

  template<typename Element>
  void
  QueueWithPriority<Element>::PutElementToQueue(const Element &element, ElementPriority priority)
  {
    container_[priority].push_back(element);
  }

  template<typename Element>
  bool QueueWithPriority<Element>::isEmpty() const noexcept
  {
    return container_[0].empty() && container_[1].empty() && container_[2].empty();
  }

  template<typename Element>
  void QueueWithPriority<Element>::clear() noexcept
  {
    for (auto &i: container_)
    {
      i.clear();
    }
  }

  template<class Element>
  Element QueueWithPriority<Element>::GetElementFromQueue()
  {
    if (!container_[2].empty())
    {
      auto tmp = container_[2].front();
      container_[2].pop_front();
      return tmp;
    } else if (!container_[1].empty())
    {
      auto tmp = container_[1].front();
      container_[1].pop_front();
      return tmp;
    } else if (!container_[0].empty())
    {
      auto tmp = container_[0].front();
      container_[0].pop_front();
      return tmp;
    }
    Element wrong;
    return wrong;
  }

  template<class Element>
  void QueueWithPriority<Element>::Accelerate()
  {
    if (!container_[ElementPriority::LOW].empty())
    {
      container_[ElementPriority::HIGH].insert(container_[ElementPriority::HIGH].end(),
                                               container_[ElementPriority::LOW].begin(),
                                               container_[ElementPriority::LOW].end());
      container_[ElementPriority::LOW].clear();
    }
  }
}

#endif
