#include <iostream>
#include <regex>
#include <string>
#include "QueueWithPriority.hpp"

namespace detail
{
  struct commandSet
  {
    enum command_
    {
      ADD,
      GET,
      ACCELERATE,
      NO
    };
    command_ c_;
    ElementPriority p_;
    std::string element;
  };

  //definition in cpp file
  commandSet getCommand(std::string &command);

  template <typename Element>
  void commandParser(commandSet &command, Element &element, std::ostream &out)
  {
    switch (command.c_)
    {
      case commandSet::command_::ADD:
      {
        element.PutElementToQueue(command.element, command.p_);
        break;
      }
      case commandSet::command_::GET:
      {
        if (!element.isEmpty())
        {
          out << element.GetElementFromQueue() << std::endl;
        }
        else
        {
          out << "<EMPTY>" << std::endl;
        }
        break;
      } 
      case commandSet::command_::ACCELERATE:
      {
        element.Accelerate();
        break;
      }
      default:
      {
        out << "<INVALID COMMAND>" << std::endl;
      }
    }
  }
}
