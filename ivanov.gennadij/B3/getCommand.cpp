#include "CommandParser.hpp"

namespace detail
{
  commandSet getCommand(std::string &command)
  {
    std::cmatch res;
    const std::regex addHigh("^(add)[[:space:]](high)[[:space:]][A-Za-zА-Яа-я0-9_]{1,}",
                             std::regex::extended | std::regex::nosubs);
    const std::regex addNormal("^(add)[[:space:]](normal)[[:space:]][A-Za-zА-Яа-я0-9_]{1,}",
                               std::regex::extended | std::regex::nosubs);
    const std::regex addLow("^(add)[[:space:]](low)[[:space:]][A-Za-zА-Яа-я0-9_]{1,}",
                            std::regex::extended | std::regex::nosubs);
    const std::regex get("^(get)$", std::regex::extended | std::regex::nosubs);
    const std::regex accelerate("^(accelerate)$", std::regex::extended | std::regex::nosubs);
    if (std::regex_search(command.c_str(), res, addHigh))
    {
      return {commandSet::ADD, ElementPriority ::HIGH, command.substr(9)};
    }
    else if (std::regex_search(command.c_str(), res, addNormal))
    {
      return {commandSet::ADD, ElementPriority ::NORMAL, command.substr(11)};
    }
    else if (std::regex_search(command.c_str(), res, addLow))
    {
      return {commandSet::ADD, ElementPriority ::LOW, command.substr(8)};
    }
    else if (std::regex_search(command.c_str(), res, get))
    {
      return {commandSet::GET};
    }
    else if (std::regex_search(command.c_str(), res, accelerate))
    {
      return {commandSet::ACCELERATE};
    }
    else
    {
      return {commandSet::NO};
    }
  }
}
