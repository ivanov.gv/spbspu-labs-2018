#ifndef COMPONENT_HPP
#define COMPONENT_HPP

#include <string>

struct component_t
{
  enum type_t
  {
    WORD,
    NUMBER,
    PUNCTUATION,
    DASH,
    SPACE
  };
  
  std::string value;
  type_t type;
};

#endif //COMPONENT_HPP
