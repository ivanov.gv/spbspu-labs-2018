#ifndef OUTPUTITEM_HPP
#define OUTPUTITEM_HPP

#include <iostream>
#include <list>
#include "component.hpp"

void print (std::ostream & out, const std::list<component_t> & line);
int printReform (std::ostream & out, std::list<component_t> & line);
void outFormat(std::ostream & out, unsigned int width, const std::list<component_t> & vector);
bool trueCourse (const std::list<component_t> & vector);

#endif //OUTPUTITEM_HPP
