#include "InputItem.hpp"
#include <locale>

InputEditor::InputEditor(std::istream & input_stream) :
  input_stream_(input_stream)
{}

void InputEditor::editInput()
{
  while(input_stream_)
  {
    readElement();
  }
}

std::list<component_t>::iterator InputEditor::begin()
{
  return text_.begin();
}

std::list<component_t>::iterator InputEditor::end()
{
  return text_.end();
}

void InputEditor::readWord()
{
  component_t co { "", component_t::WORD };
  do
  {
    char symbol = input_stream_.get();
    co.value.push_back(symbol);
    
    if(symbol == '-' && input_stream_.peek() == '-')
    {
      co.value.pop_back();
      input_stream_.unget();
      break;
    }
    
  } while ((std::isalpha<char>(input_stream_.peek(), std::locale())) || (input_stream_.peek() == '-'));
  text_.push_back(co);
}

void InputEditor::readNumber()
{
  component_t co { "", component_t::NUMBER };
  char point = std::use_facet<std::numpunct<char>>(std::locale()).decimal_point();
  bool pointRead = false;
  do
  {
    char symbol = input_stream_.get();
    if(symbol == point)
    {
      if(pointRead)
      {
        input_stream_.unget();
        break;
      }
      pointRead = true;
    }
    co.value.push_back(symbol);
  } while((std::isdigit<char>(input_stream_.peek(), std::locale()) || (input_stream_.peek() == point)));

  text_.push_back(co);
}

void InputEditor::readDash()
{
  component_t co { "", component_t::DASH };
  while(input_stream_.peek() == '-')
  {
    char symbol = input_stream_.get();
    co.value.push_back(symbol);
  }
  text_.push_back(co);
}

void InputEditor::readElement()
{
  char symbol = input_stream_.get();
  while(std::isspace(symbol, std::locale()))
  {
    symbol = input_stream_.get();
  }
  
  if (isalpha(symbol, std::locale()))
  {
    input_stream_.unget();
    readWord();
  }
  else if (symbol == '-')
  {
    if (input_stream_.peek() == '-')
    {
      input_stream_.unget();
      readDash();
    }
    else
    {
      input_stream_.unget();
      readNumber();
    }
  }
  else if ((symbol == '+' || (isdigit(symbol, std::locale()))))
  {
    input_stream_.unget();
    readNumber();
  }
  else if (ispunct(symbol, std::locale()))
  {
    component_t co { "", component_t::PUNCTUATION };
    co.value.push_back(symbol);
    text_.push_back(co);
  }
}
