#include "OutputItem.hpp"
#include <list>

void print (std::ostream & out, const std::list<component_t> & line)
{
  for (auto iter = line.begin(); iter != line.end(); iter++)
  {
    out << (*iter).value;
  }
  out << std::endl;
}

int printReform (std::ostream & out, std::list<component_t> & line)
{
  std::size_t width = 0;
  std::list<component_t> new_line;
  while (!line.empty())
  {
    new_line.push_front(line.back());
    width += line.back().value.size();
    line.pop_back();
    if (new_line.front().type == component_t::WORD || new_line.front().type == component_t::NUMBER)
    {
      break;
    }
  }
  print(out, line);
  line = new_line;
  return width;
}

void outFormat (std::ostream & out, unsigned int width, const std::list<component_t> & vector)
{
  std::size_t currentWidth = 0;
  std::list<component_t> line;
  for(auto iter = vector.begin(); iter != vector.end(); iter++)
  {
    switch((*iter).type)
    {
    case component_t::PUNCTUATION:
      if(currentWidth + 1 > width)
      {
        currentWidth = printReform(out, line);
      }
      line.push_back(*iter);
      currentWidth += (*iter).value.size();
      break;
    
    case component_t::DASH:
      if(currentWidth + 4 > width)
      {
        currentWidth = printReform(out, line);
      }
      line.push_back(component_t { " ", component_t::SPACE });
      line.push_back(*iter);
      currentWidth += (*iter).value.size() + 1;
      break;
    
    case component_t::WORD:

    case component_t::NUMBER:
      if(currentWidth + (*iter).value.size() + 1 > width)
      {
        print(out, line);
        line.clear();
        currentWidth = 0;
      }
      else if(!line.empty())
      {
        line.push_back(component_t { " ", component_t::SPACE });
        currentWidth++;
      }
      line.push_back(*iter);
      currentWidth += (*iter).value.size();
      break;

    case component_t::SPACE:
      break;
    }

  }
  if(!line.empty())
  {
    print(out, line);
  }
}

bool trueCourse(const std::list<component_t> & vector)
{
  if(!vector.empty() && (vector.front().type != component_t::WORD) && (vector.front().type != component_t::NUMBER))
  {
    return false;
  }
  for(auto iter = vector.begin(); iter != vector.end(); iter++)
  {
    switch((*iter).type)
    {
    case component_t::WORD:

    case component_t::NUMBER:
      if((*iter).value.size() > 20)
      {
        return false;
      }
      break;

    case component_t::DASH:
      if((*iter).value.size() != 3)
      {
        return false;
      }
      if(iter != vector.begin())
      {
        const component_t &prev = *std::prev(iter);
        if((prev.type == component_t::DASH) || ((prev.type == component_t::PUNCTUATION) && (prev.value != ",")))
        {
          return false;
        }
      }
      break;

    case component_t::PUNCTUATION:
      if(iter != vector.begin())
      {
        const component_t &prev = *std::prev(iter);
        if((prev.type == component_t::DASH) || (prev.type == component_t::PUNCTUATION))
        {
          return false;
        }
      }
      break;
      
    case component_t::SPACE:
      break;
    }
  }
  return true;
}
