#include <iostream>
#include "InputItem.hpp"
#include "OutputItem.hpp"

int main(int args, char * argv[])
{
  int width = 40;
  
  if (args > 1)
  {
    if (args != 3)
    {
      std::cerr << "Incorrect amount of parameters";
      return 1;
    }
    else if (std::string(argv[1]) != "--line-width")
    {
      std::cerr << "Incorrect arguments";
      return 1;
    }
    else
    {
      width = std::stoi(argv[2]);
      if (width < 24)
      {
        std::cerr << "Incorrect line width";
        return 1;
      }
    }
  }
  
  try 
  {
    InputEditor editor(std::cin);
    editor.editInput();
    
    std::list<component_t> vector(editor.begin(), editor.end());

    if (!trueCourse(vector))
    {
      throw std::invalid_argument("Incorrect input");
    }
    
    outFormat(std::cout, width, vector);
  }
  
  catch (const std::invalid_argument & err)
  {
    std::cerr << err.what();
    return 1;
  }
  
  return 0;
}
