#ifndef INPUTITEM_HPP
#define INPUTITEM_HPP

#include <iostream>
#include <list>
#include "component.hpp"

class InputEditor
{
public:
  InputEditor(std::istream & input_stream);
  void editInput();
  std::list <component_t>::iterator begin();
  std::list <component_t>::iterator end();
  
private:
  std::list<component_t> text_;
  std::istream & input_stream_;
  void readElement();
  void readWord();
  void readNumber();
  void readDash();
};

#endif //INPUTITEM_HPP
