#include <iostream>
#include <vector>
#include <functional>
#include <cstring>
#include <forward_list>
#include <memory>
#include <fstream>
#include <random>
#include "tasks.h"

int main (int argc, char *argv[])
{
  if (argc < 2)
  {
    std::cerr << "Not enough arguments" << std::endl;
    return 1;
  }
  try
  {
    int taskNumber = std::stoi(argv[1]);
    switch (taskNumber)
    {
      case 1:
      {
        if (argc == 3)
        {
          firstTask(argv[2], std::cin);
        }
        else
        {
          throw std::invalid_argument("Wrong arguments input");
        }
        break;
      }

      case 2:
      {
        if (argc == 3)
        {
          secondTask(argv[2]);
        }
        else
        {
          throw std::invalid_argument("Wrong arguments input");
        }
        break;
      }

      case 3:
      {
        if (argc == 2)
        {
          thirdTask(std::cin);
        }
        else
        {
          std::invalid_argument("Wrong arguments input");
        }
        break;
      }

      case 4:
      {
        if (argc == 4)
        {
          unsigned long long int vectorSize = std::stoi(argv[3]);
          fourthTask(vectorSize, argv[2]);
        }
        else
        {
          throw std::invalid_argument("Wrong arguments input");
        }
        break;
      }

      default:
      {
        throw std::invalid_argument("Wrong arguments input");
      }
    }
  }
  catch (const std::exception &exception)
  {
    std::cerr << exception.what() << std::endl;
    return 1;
  }
  return 0;
}
