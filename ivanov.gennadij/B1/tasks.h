#ifndef B1_TASKS_H
#define B1_TASKS_H

#include <iostream>
#include <string>

void firstTask (const char *direction, std::istream &inStream);

void secondTask (const std::string &file);

void thirdTask (std::istream &inStream);

void fourthTask (const unsigned long long int &vectorSize, const char *direction);


#endif
