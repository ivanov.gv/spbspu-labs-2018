#ifndef B1_ACCESSTYPES_H
#define B1_ACCESSTYPES_H

#include <cstddef>

namespace accesstypes
{
  template<class Container>
  struct BracketsAccess
  {
    typename Container::reference getElement (Container &container, std::size_t i)
    {
      return container[i];
    }

    static std::size_t getBegin (Container &)
    {
      return 0;
    }

    static std::size_t getEnd (Container &container)
    {
      return container.size();
    }
  };

  template<class Container>
  struct AtAccess
  {
    typename Container::reference getElement (Container &container, size_t i)
    {
      return container.at(i);
    }

    static std::size_t getBegin (Container &)
    {
      return 0;
    }

    static std::size_t getEnd (Container &container)
    {
      return container.size();
    }
  };

  template<class Container>
  struct IteratorAccess
  {
    typename Container::reference getElement (Container &, typename Container::iterator &element)
    {
      return *element;
    }

    typename Container::iterator getBegin (Container &container)
    {
      return container.begin();
    }

    typename Container::iterator getEnd (Container &container)
    {
      return container.end();
    }
  };
}

#endif
