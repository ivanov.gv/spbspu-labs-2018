#include <vector>
#include <forward_list>
#include <functional>
#include <cstring>
#include <fstream>
#include <memory>
#include "accesstypes.h"
#include "functions.h"
#include "tasks.h"

void firstTask (const char *direction, std::istream &inStream)
{
  std::vector<int> vector1(0);
  int i = 0;
  while (inStream >> i)
  {
    vector1.push_back(i);
  }
  if (!inStream.eof())
  {
    throw std::ios_base::failure("Wrong characters");
  }
  if (!vector1.empty())
  {
    std::vector<int> vector2(vector1.begin(), vector1.end());
    std::forward_list<int> list(vector1.begin(), vector1.end());
    if (std::strcmp(direction, "descending") == 0)
    {
      detail::sort<accesstypes::BracketsAccess>(vector1, std::less<int>());
      detail::sort<accesstypes::AtAccess>(vector2, std::less<int>());
      detail::sort<accesstypes::IteratorAccess>(list, std::less<int>());
    } else if (std::strcmp(direction, "ascending") == 0)
    {
      detail::sort<accesstypes::BracketsAccess>(vector1, std::greater<int>());
      detail::sort<accesstypes::AtAccess>(vector2, std::greater<int>());
      detail::sort<accesstypes::IteratorAccess>(list, std::greater<int>());
    } else
    {
      throw std::invalid_argument("Wrong sorting order");
    }

    detail::print(vector1);
    detail::print(vector2);
    detail::print(list);

  } else
  {
    if (std::strcmp(direction, "ascending") == 0 || std::strcmp(direction, "descending") == 0)
    {
      return;
    } else
    {
      throw std::invalid_argument("Wrong sorting order");
    }
  }
}

void secondTask (const std::string &file)
{
  std::ifstream in(file);
  if (!in)
  {
    throw std::invalid_argument("File does not exist!");
  }
  in.seekg(0, std::ios_base::end);
  auto size = in.tellg();
  in.seekg(0);
  try
  {
    std::unique_ptr<char[]> array(new char[size]);
    int j = 0;
    while (j != size && !in.eof() && in)
    {
      in >> std::noskipws >> array[j];
      ++j;
    }
    if ((in.eof() && j + 1 < size) || (!in.eof() && j + 1 == size))
    {
      throw std::ios_base::failure("File is corrupted or changed");
    }
    std::vector<char> vec(&array[0], &array[j]);
    for (auto &i : vec)
    {
      std::cout << i;
    }
  }
  catch (const std::bad_alloc &noMemory)
  {
    throw noMemory;
  }
}

void thirdTask (std::istream &inStream)
{
  std::vector<int> vector1(0);
  int lastElement = 1;
  while (!(inStream >> lastElement).eof())
  {
    if (inStream.fail() || inStream.bad())
    {
      throw std::ios_base::failure("Wrong characters.");
    }
    if (lastElement != 0)
    {
      vector1.push_back(lastElement);
    }
    else
    {
      break;
    }
  }
  if (!vector1.empty() && lastElement != 0)
  {
    throw std::invalid_argument("Last element must be zero.");
  } else if (vector1.empty())
  {
    return;
  }

  switch (vector1.back())
  {
    case 1:
    {
      for (auto i = vector1.begin(); i != vector1.end(); i++)
      {
        if (*i % 2 == 0)
        {
          i = --vector1.erase(i);
        }
      }
      break;
    }
    case 2:
    {
      for (auto i = vector1.begin(); i != vector1.end(); i++)
      {
        if (*i % 3 == 0)
        {
          i = vector1.insert(++i, 3, 1);
        }
      }
      break;
    }
    default:
      break;
  }
  for (auto i: vector1)
  {
    std::cout << i << ' ';
  }
}

void fourthTask (const unsigned long long int &vectorSize, const char *direction)
{
  std::vector<double> vec(vectorSize);
  detail::fillRandom(&vec[0], vec.size());
  if (std::strcmp(direction, "ascending") == 0)
  {
    detail::print(vec);
    detail::sort<accesstypes::BracketsAccess>(vec, std::greater<double>());
  } else if (std::strcmp(direction, "descending") == 0)
  {
    detail::print(vec);
    detail::sort<accesstypes::BracketsAccess>(vec, std::less<double>());
  } else
  {
    throw std::length_error("Wrong sorting order");
  }
  detail::print(vec);
}
