#ifndef B1_FUNCTIONS_H
#define B1_FUNCTIONS_H

#include <iostream>
#include <vector>
#include <functional>
#include <random>


namespace detail
{
  template<template<class> class AccessType, class Container, class Compare>
  void sort (Container &container1, const Compare &comparator)
  {
    auto n = AccessType<Container>().getEnd(container1);
    for (auto i = AccessType<Container>().getBegin(container1); i != n; i++)
    {
      for (auto j = i; j != n; j++)
      {
        if (comparator(AccessType<Container>().getElement(container1, i),
                       AccessType<Container>().getElement(container1, j)))
        {
          std::swap(AccessType<Container>().getElement(container1, i),
                    AccessType<Container>().getElement(container1, j));
        }
      }
    }
  }

  template<class Container>
  void print (Container &container)
  {
    for (auto &i : container)
    {
      std::cout << i << " ";
    }
    std::cout << std::endl;
  }

  void fillRandom (double *array, int size)
  {
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<double> dist(-1.0, 1.0);
    for (int i = 0; i < size; ++i)
    {
      array[i] = dist(mt);
    }
  }
}

#endif
